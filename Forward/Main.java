/**
 * Implementation of the Forward algorithm that simply handles coin flip cases, 
 * but is written to support any cases with discrete values.
 * 
 * @author Erick Pulido
 * @course CS582 - INTRO TO SPEECH PROCESSING
 * @due	   13 March 2019		
 */
public class Main {
	/**
	 * Performs the Forward algorithm to efficiently find the probability
	 * of an observation for a given model.
	 * @param	A			The transition probabilities
	 * @param	B			Observation symbol probability distribution
	 * @param	Pi			Probability that given state is initial
	 * @param	Obv			The observation sequence
	 * @return	obv_given_lambda	P(O | Lambda)
	 */
	public static double Forward(double A[][], double B[][], 
				     double Pi[][], String Obv[]) {
		int state;
		int initial_state;
		double obv_given_lambda = 0.0;
		double alpha[][] = new double[Obv.length][Obv.length];
		final String HEADS = "H";
		/* Initialization
		* alpha_1 (i) = pi_i * b_i (o_1), where 1 <= i <= N
		*/
		if(Obv[0].equals(HEADS)) initial_state = 0;
		else initial_state = 1;		
		for(int j = 0; j < B[0].length; j++)
			alpha[0][j] = Pi[j][0] * B[initial_state][j];	
		/* Induction
		*  alpha_(t+1) (j) = [Sum from i = 1 to N of alpha_t(i) * a_ij] 
		*  * b_j (O_(t+1)) where 1 <= t <= T-1 and 1 <= j <= N
		*/
		for(int i = 1; i < Obv.length; i++) {
			if(Obv[i].equals(HEADS)) state = 0;
			else state = 1;
			for(int j = 0; j < A.length; j++)
				alpha[i][j] = ((alpha[i-1][0] * A[0][j]) 
							+ (alpha[i-1][1] * A[1][j]))
							* B[state][j];
			obv_given_lambda = alpha[i][0] + alpha[i][1];
		}
		return obv_given_lambda;
	}
	/* Simple driver with tests */
	public static void main(String[] args) {
		double A[][] = {
				{0.2, 0.8}, 	// A_1,1 and A_1,2
				{0.6, 0.4}	// A_2,1 and A_2,2
		};
		double B[][] = {
				{0.7, 0.4}, 	// P(H)
				{0.3, 0.6}	// P(T)
		};
		double Pi[][] = {
				{1.0},		// pi_1 (1)
				{0.0}		// pi_1 (2)
		};
		double p_obv_given_lambda = 0.0;
		
		/* The following two are NOT part of the assignment description.
		 * They were mostly for my benefit to further test my implementation.
		 * From CS582 Lecture Slides and HMM Homework by Chuck Konopka 
		 */
		String Obv_Ex1[] = {"H","T","T"};
		p_obv_given_lambda = Forward(A, B, Pi, Obv_Ex1);
		System.out.println("Obv_Ex1 P(Obv|Lambda) = " + p_obv_given_lambda);
		
		String Obv_Ex2[] = {"H","H","T"};	
		p_obv_given_lambda = Forward(A, B, Pi, Obv_Ex2);
		System.out.println("Obv_Ex2 P(Obv|Lambda) = " + p_obv_given_lambda);
		
		/* The following are FOR the homework assignment.
		 * Custom three observation vector.
		 */
		String Obv_1[] = {"T", "H", "T"};
		p_obv_given_lambda = Forward(A, B, Pi, Obv_1);
		System.out.println("Obv_1 P(Obv|Lambda) = " + p_obv_given_lambda);
		
		/* Seven plus observations vectors */
		String Obv_2[] = {"H", "T", "H", "T", "H", "T", "H", "T", "T", "H"}; 
		p_obv_given_lambda = Forward(A, B, Pi, Obv_2);
		System.out.println("Obv_2 P(Obv|Lambda) = " + p_obv_given_lambda);
		
		String Obv_3[] = {"T", "H", "T", "T", "H", "H", "H"}; 
		p_obv_given_lambda = Forward(A, B, Pi, Obv_3);
		System.out.println("Obv_3 P(Obv|Lambda) = " + p_obv_given_lambda);
		
		String Obv_4[] = {"H", "T", "H", "H", "T", "T", "T"}; 
		p_obv_given_lambda = Forward(A, B, Pi, Obv_4);
		System.out.println("Obv_4 P(Obv|Lambda) = " + p_obv_given_lambda);
		
	}
}
