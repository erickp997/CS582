'''
 * Implementation of the Viterbi algorithm that simply handles coin flip cases, 
 * but is written to support any cases with discrete values.
 * 
 * @author Erick Pulido
 * @course CS582 - INTRO TO SPEECH PROCESSING    
'''
'''
 * Performs the Viterbi algorithm to efficiently find the best path
 * and score of an observation for a given model.
 * @param   A                   The transition probabilities
 * @param   B                   Observation symbol probability distribution
 * @param   Pi                  Probability that given state is initial
 * @param   Obv                 The observation sequence
 * @param   FSA_states          The list of states for this algorithm
 * @return  viterbi_score       The score of the best path
 * @return  psi_values[state]   The best or most likely path     
'''
def Viterbi(A, B, Pi, Obv, FSA_states):
    delta_values = [{}] # Make use of list structure
    psi_values = {} # Make use of dictionary structure
    '''
    Initialization
    * delta_1 (i) = pi_i * b_i (o_1) and psi_1 (i) = 0 
    * where 1 <= i <= N
    '''
    for i in FSA_states:
        delta_values[0][i] = Pi[i] * B[i][Obv[0]]
        psi_values[i] = [i]
    '''
    Induction
    *  delta_t (j) = max[delta_(t-1) (i) * a_ij] b_j * (o_t),
    *  psi_t (j) = argmax[delta_(t-1) (i) * a_ij]
    '''
    for i in range(1,len(Obv)):
        delta_values.append({})
        new_psi = {}
        for j in FSA_states:
            '''
            Using the max functions built in mode:
            max(iterable, *[, default=obj, key=func]) -> value
            Convenient and does not really exist in Java.
            '''
            (viterbi_score, status) = max(
                [(delta_values[i-1][k] * A[k][j] * B[j][Obv[i]], k) 
                 for k in FSA_states])
            delta_values[i][j] = viterbi_score
            new_psi[j] = psi_values[status] + [j]
            ''' For debugging purposes only
            for k in FSA_states:
                print("delta_values[i-1][k] = ", delta_values[i-1][k])
                print("A[k][j] = ", A[k][j])
                print("B[j][Obv[i] = ", B[j][Obv[i]])
                print("delta * A * B = ", 
                      delta_values[i-1][k] * A[k][j] * B[j][Obv[i]])
                print("k = ", k)
            '''
        psi_values = new_psi  
    # Termination  
    (viterbi_score, status) = max(
        [(delta_values[len(Obv) - 1][t], t) 
         for t in FSA_states])
    return (viterbi_score, psi_values[status]) # Return as tuple 

### Simple driver that performs Viterbi on different observation vectors ###
def main():
    ## Testing with example from lecture slides to verify correctness ## 
    A = {   # Use of dictionary made manipulation easier to work with
       '1' : {'1': 0.2, '2': 0.8}, # A_1,1 , A_1,2
       '2' : {'1': 0.6, '2': 0.4}  # A_2,1 , A_2,2
   }
    B = {
       '1' : {'H': 0.7, 'T': 0.3},
       '2' : {'H': 0.4, 'T': 0.6}  
    }
    FSA_states = ('1', '2') # FSA for coin flip has two states
    Obv = ('H', 'T')
    Pi = {'1': 1.0, '2': 0.0}
    print ("Testing with ", Obv, " = (value and [path])")
    print
    print (Viterbi(A, B, Pi, Obv, FSA_states))
     
    # Not part of assignment, but another test ##
    Obv = ('H', 'H', 'T')	
    print ("Testing with ", Obv, " = (value and [path])")
    print
    print (Viterbi(A, B, Pi, Obv, FSA_states))

    ## Observation vectors from Forward algorithm assignment ## 
    Obv = ('T', 'H', 'T')
    print ("Testing with ", Obv, " = (value and [path])")
    print
    print (Viterbi(A, B, Pi, Obv, FSA_states))
    
    Obv = ('H', 'T', 'H', 'T', 'H', 'T', 'H', 'T', 'T', 'H')    
    print ("Testing with ", Obv, " = (value and [path])")
    print
    print (Viterbi(A, B, Pi, Obv, FSA_states))
    
    Obv = ('T', 'H', 'T', 'T', 'H', 'H', 'H')    
    print ("Testing with ", Obv, " = (value and [path])")
    print
    print (Viterbi(A, B, Pi, Obv, FSA_states))
    
    Obv = ('H', 'T', 'H', 'H', 'T', 'T', 'T')    
    print ("Testing with ", Obv, " = (value and [path])")
    print
    print (Viterbi(A, B, Pi, Obv, FSA_states))
    
main()